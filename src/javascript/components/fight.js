import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    //initial setup

    firstFighter.isDefense = false;
    firstFighter.isCombo = false;
    firstFighter.currentHealth = firstFighter.health;
    firstFighter.health_bar = document.getElementById('left-fighter-indicator');
    secondFighter.isDefense = false;
    secondFighter.isCombo = false;
    secondFighter.currentHealth = secondFighter.health;
    secondFighter.health_bar = document.getElementById('right-fighter-indicator');

    const attack = (attack, defense) => {
      const damage = getDamage(attack, defense);
      defense.currentHealth -= damage;
      let result = defense.currentHealth /defense.health * 100;
      console.log(defense.currentHealth);
      console.log(damage);
      console.log(result);
      let newHealth = result.toString() + '%';
      attack.health_bar.style.width = newHealth;
      if (defense.currentHealth > 0) {

      } else {
        resolve(defense);
      }
    }

    document.addEventListener('keyup', event => {
      switch (event.code) {
        case controls.PlayerOneAttack:

          if (!firstFighter.isBlock && !secondFighter.isBlock){
            attack(firstFighter, secondFighter);
          }
          break;
        case controls.PlayerOneBlock:
          firstFighter.isBlock = false;
          break;
        case controls.PlayerTwoAttack:
          if (!firstFighter.isBlock && !secondFighter.isBlock) {
            attack(secondFighter, firstFighter);
          }
          break;
        case controls.PlayerTwoBlock:
          secondFighter.isBlock = false;
          break;
        default:
          break;

      }
    });

    document.addEventListener('keydown', event => {
      if ( event.repeat )  return;
      switch (event.code) {
        case controls.PlayerOneBlock:
          firstFighter.isAttack = true;
          break;
        case controls.PlayerOneCriticalHitCombination:
          // try combo
          break;
        case controls.PlayerTwoBlock:
          secondFighter.isAttack = true;
          break;
        case controls.PlayerTwoCriticalHitCombination:
          // try combo
          break;
        default:
          break;

      }
    });

  });
}

export function getDamage(attacker, defender) {
  return ((getHitPower(attacker) > getBlockPower(defender)) ? (getHitPower(attacker) - getBlockPower(defender)) : 0);
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  const power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  const power = fighter.defense * dodgeChance;
  return power;
}
