import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';

  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  if (fighter) {

    const fighterImage = createFighterImage(fighter);

    const attack = createElement({tagName: 'div'});
    const defense = createElement({tagName: 'div'});
    const health = createElement({tagName: 'div'});
    const name = createElement({tagName: 'div'});

    attack.innerText = `Attack: ${fighter.attack}`;
    defense.innerText = `Defense: ${fighter.defense}`;
    health.innerText = `Health: ${fighter.health}`;
    name.innerText = `Name: ${fighter.name}`;

    attack.style.color = "white";
    defense.style.color = "white";
    health.style.color = "white";
    name.style.color = "white";

    fighterElement.append(fighterImage, attack, defense, health, name);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
